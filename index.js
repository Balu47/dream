const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const db = require('./server/conn/mongo');
const mongodb = require('mongodb');
const admodel = require('./server/model/admodel');
const aws = require('./server/aws/dyanamo')
const mng = require('./server/mongo/mongoos')

const port = 3000;

const app = express();

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use('/aws', aws);
app.use('/mongo', mng);


app.get('/', function(req, res){
  res.send("Welcome to node.js")
})

app.get('*', (req, res) => {
  res.send("404 Not Found");
})

app.post('*', (req, res) => {
  res.send("404 Not Found");
})

app.listen(process.env.PORT || port, function(){
  console.log("Server running on " + port)
})
