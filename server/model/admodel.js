const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const ads = new Schema({

    title: { type:String, required:true},
    cat: { type:String, required:true },
    loc: { type:String, required:true},
    price: { type:String, required:true},
    desc: {type:String, required:true},
    im: []

});

var admodel = mongoose.model('ads', ads)
module.exports = admodel
