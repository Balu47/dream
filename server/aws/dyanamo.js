const express = require('express');
const router = express.Router();
var AWS = require("aws-sdk");

AWS.config.update({region: "ap-south-1"});

var credentials = new AWS.SharedIniFileCredentials();
AWS.config.credentials = credentials;

var ddb = new AWS.DynamoDB.DocumentClient();

router.post('/add', (req, res) => {

var ims = []
req.body.im.forEach(function(imms){
  ims.push(imms)
});

  var params = {
      TableName:'ads',
      Item:{
          "user_id": req.body.user_id,
          "price": req.body.price,
          "cat": req.body.cat,
          "loc": req.body.loc,
          "im": ims
      }
  };

  console.log("Adding a new item...");
  ddb.put(params, function(err, data) {
      if (err) {
          res.send(err)
          // console.error("Unable to add item. Error JSON:", JSON.stringify(err, null, 2));
      } else {
          res.send(data)
          // console.log("Added item:", JSON.stringify(data, null, 2));
      }
  });

})

router.post('/get', (req, res) => {

  var params = {
      TableName: 'ads',
      Key:{
          "cat": req.body.cat,
          "loc": req.body.loc
      }
  };

  ddb.get(params, function(err, data) {
      if (err) {
        res.send(err)
          // console.error("Unable to read item. Error JSON:", JSON.stringify(err, null, 2));
      } else {

    res.send(data.Item);
          // console.log("GetItem succeeded:", JSON.stringify(data, null, 2));
      }
  });

})

router.get('/query', (req, res) => {


  var params = {
      TableName : "ads",
      KeyConditionExpression: "#user_id = :id",
      ExpressionAttributeNames:{
          "#user_id": "user_id"
      },
      ExpressionAttributeValues: {
          ":id": req.query.id
      }
  };

  ddb.query(params, function(err, data) {
      if (err) {

        res.send(err)
      } else {

    res.send(data);
      }
  });
})

router.get('/gsi', (req, res) => {


  var params = {
      TableName : "ads",
      IndexName : "cat-loc-index",
      Limit : 3,
      ScanIndexForward:true,
      KeyConditionExpression: "#cat = :cat and #loc > :loc",
      ExpressionAttributeNames:{
          "#cat": "cat",
          "#loc": "loc"
      },
      ExpressionAttributeValues: {
          ":cat": req.query.cat,
          ":loc": req.query.loc
      }
  };

  ddb.query(params, function(err, data) {
      if (err) {

        res.send(err)
      } else {

    res.send(data);
      }
  });
})

router.get('/scan', (req, res) => {


  var params = {
      TableName : "ads",
      Limit: req.query.limit
  };

  ddb.scan(params, onScan);
  var datas = [];

function onScan(err, data){
  if(err) {
    res.send(err)
  }else{

    data.Items.forEach(function(move){
      datas.push(move)
    });

    if (typeof data.LastEvaluatedKey != "undefined") {
            console.log("Scanning for more...");
            params.ExclusiveStartKey = data.LastEvaluatedKey;
            ddb.scan(params, onScan);
        }
        else{
      res.send(datas)
            }

  }
}

})

router.post('/update', (req, res) => {
  var params = {
    TableName: 'ads',
    Key:{
        "cat": req.body.cat,
        "loc": req.body.loc
    },
    UpdateExpression: "set info.rating = :r, info.plot=:p, info.actors=:a",
    ExpressionAttributeValues:{
        ":r":5.5,
        ":p":"Everything happens all at once.",
        ":a":["Larry", "Moe", "Curly"]
    },
    ReturnValues:"UPDATED_NEW"
};

ddb.update(params, function(err, data) {
    if (err) {
      res.send(err)
    } else {
      res.send(data)
    }
});
})

router.post('/updateone', (req, res) => {

var params = {
  TableName: 'ads',
  Key:{
      "cat": req.body.cat,
      "loc": req.body.loc
  },
    UpdateExpression: "set info.actors[0] = :r",
    ConditionExpression: "size(info.actors) >= :num",
    ExpressionAttributeValues:{
        ":num": 2,
        ":r":req.body.ims
    },
    ReturnValues:"UPDATED_NEW"
};

ddb.update(params, function(err, data) {
    if (err) {
      res.send(err)
    } else {
      res.send(data)
    }
});
})

module.exports = router;
