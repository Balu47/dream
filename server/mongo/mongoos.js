const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const db = require('../conn/mongo');
const mongodb = require('mongodb');
const admodel = require('../model/admodel');
const router = express.Router();


router.post('/ads', (req, res) => {
  var title = req.body.title;
  var cat = req.body.cat;
  var loc = req.body.loc;
  var price = req.body.price;
  var desc = req.body.desc;

const ads = new admodel({
  title: title,
  cat: cat,
  loc: loc,
  price: price,
  desc: desc,
  im: req.body.im
})

ads.save()
   .then(ads =>{
     res.send('success')
   })
   .catch(err =>{
     res.send(err)
   })


})



router.get('/ads',(req, res) => {

   db.collection('ads').find().toArray(function(err, result){
     if(err){
       res.json({success: false,
                 error:err})
     }else{
       res.json({success: true,
                 data: result})
     }
   })

})


router.post('/findads',(req, res) => {

   db.collection('ads').find({cat: req.body.name}).toArray(function(err, result){
     if(err){
       res.json({success: false,
                 error:err})
     }else{
       res.json({success: true, data: result})
     }
   })

})

router.post('/adim',(req, res) => {
  var id = req.body.id;
  var im = req.body.im;

  db.collection('ads')
    .update({ _id: new mongodb.ObjectID(id)}
            , {$push: {im: im}}, function(err, re){
              if (err) return res.send(err);
              res.send("Success "+re)
            })

  db.close();

})

module.exports = router;
